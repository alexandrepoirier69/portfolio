= Stages Evry
:author: Alexandre Poirier
:docdate: 2022-05-03
:asciidoctor-version:1.1
:description: Compte rendu du Projet GeoWorld
:toc-title: Table des matières
:toc: left
:toclevels: 5
:listing-caption: Listing
:icons: font

== Présentation
Dans la période du 03 janvier 2023 au 10 févirer 2023 :

Lors de ma première mission la consigne était :

* Parser les flux XML donné, en PHP car pour la seconde mission le projet est sous le framwork Symfony, pour afficher les secrétariats pédagogique

image::assets/img/xml.png[]

image::assets/img/filiere.png[]

Ma deuxième mission était de mettre à jour une application disponible dans l'intranet de l'université :

L'application au départ ressemblait à ceci :

image::assets/img/qui&.png[]

La mise à jour consiste à ajouter un onglet pour afficher différentes informations sur le personnel qui se trouvent dans différentes bases de données :

image::assets/img/qui2.png[]

image::assets/img/qui3.png[]

