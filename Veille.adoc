= Veille Tech
:author: Alexandre Poirier
:docdate: 2023-04
:asciidoctor-version:1.1
:description: veille ia
:toc-title: Table des matières
:toc: left
:toclevels: 5
:listing-caption: Listing
:icons: font

== Présentation

Dans cette veille, je vais vous présenter l'évolution des Intelligences Artificiels (IA),
ces dernières années et ces derniers mois.

Cette veille a été réalisée Alexandre POIRIER en 2023.

<<<

== Les différentes IA

Voici un schéma des différentes IA, ainsi que leurs évolutions dans le temps :

image::assets/img/veille.png[]

=== ChatGPT
Plus le temps avance plus les IA deviennent puissantes et ont un champ
d'action très large. Nous pouvons voir cela avec l'IA la plus connue en ce moment
chatGPT. Cette IA développée par OpenIA, en python principalement, est
utilisée dans plein de domaines différents.

En effet chatGPT a un champ d'action extrêmement large, ce qui lui permet d'être présente
dans beaucoup de domain. Effectivement les usages principaux de chatGPT sont:

* Répondre à des questions.
* Classer ou extraire des données.
* Générer du code.
* Transformer des contenus.
* Générer des messages.

Or dans le cadre de chatGPT, mais aussi pour d'autres IA, le fait qu'elle ait un champ d'action ultra généralisée, cela limite la puissance de l'IA.
Pour obtenir de meilleurs certains développeurs ont intégré chatGPT à la messagerie WhatsApp,
avec différents "personna" qui ont un champ d'action limité ce qui leurs permet,
lorsqu'ils pausent des questions dans les cadres d'actions spécifiques de chaque "personna",
les réponses sont beaucoup plus précises et de meilleures qualités.

Aujourd'hui les IA ne sont pas seulement présentes dans le monde du développement.
En effet, les IA sont présentes dans différents domains tel que :

=== Le graphisme
DALL-E ou encore Midjourney sont des IA qui permettent de générer des images avec des "promts",
c'est-à-dire juste avec du texte. Plus les versions des IA avancent plus les images générées sont réalistes.

image::assets/img/midjourney-evolution-.jpg[]

=== La musique
Les IA sont aussi présentes dans le domaine de la musique avec par exemple MusicLM, créer par Google.
Dans le même principe que les autres IA, la musique est générée à partir de texte.

https://google-research.github.io/seanet/musiclm/examples/

=== Jeux vidéos
Le jeu vidéo est un milieu ou la nouvelle technologie est très souvent présente, comme pour
pousser les performances des jeux. C'est le cas par exemple pour calculer et générer de la lumière et des reflets dans les jeux.
C'est le cas avec Nvidia et sa technologie du ray tracing :

image::assets/img/RTX-ray-tracing-on-off.jpg[]

== Conclusion

L'ère des IA est très récent et son évolution n'est pas encore à son apogée.
Cette avancée ultra rapide des IA pose un problème d'un point de vu juridique.
En effet, les IA sont considérées comme des outils, ce qui signifie que les résultats
générés par l'IA sont considérés comme la propriété de la personne qui a écrit les prompts.

La pause demandée par Elon Musk et les experts, va peut-être permet à la législation de prendre en charge
les problèmes liés aux IA.

== Sources
Mes sources sont :

* Google
* TikTok
* Reddit
* Twitter


